

Module
======
System Information


Description
===========
This module displays information about the current state of the Drupal
installation and system environment.


Requirements
============
Drupal 7.x


Installation
============
1. Move this folder into your modules directory.
2. Enable it from Administration >> Modules >> Administration.

It's available now at Administration >> Reports >> System information.


Configuration
=============
Configure it at Administration >> Reports >> System information >> Settings.


Projekt page
============
http://drupal.org/project/systeminfo


Author & maintainer
===================
Ralf Stamm


License
=======
GNU General Public License (GPL)
