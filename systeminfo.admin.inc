<?php

/**
 * @file
 * Admin page callbacks for the systeminfo module.
 */


/**
 * Menu callback; retrieves overview page.
 */
function systeminfo_admin_overview() {

  drupal_add_css(drupal_get_path('module', 'systeminfo') .'/styles/systeminfo.css');

  $output = systeminfo_admin_overview_drupal();
  $output .= systeminfo_admin_overview_php();
  $output .= systeminfo_admin_overview_database();

  return $output;
}


function systeminfo_admin_overview_drupal() {

  $rows = array();
  $rows[] = array(t('Version'), VERSION);
  $rows[] = array(t('Configuration file'), conf_path() .'/settings.php');
  $cron_last = variable_get('cron_last');
  if (!is_numeric($cron_last)) {
    $cron_last = variable_get('install_time', 0);
  }
  $rows[] = array(t('Cron maintenance tasks'), t('Last run !time ago', array('!time' => format_interval(REQUEST_TIME - $cron_last))));
  $system_info = system_get_info('module', variable_get('install_profile', 'standard'));
  $rows[] = array(t('Install profile'), $system_info['name']);
  $rows[] = array(t('Install time'), format_date(variable_get('install_time', 0), 'small'));
  $rows[] = array(t('Public file system path'), variable_get('file_public_path', conf_path() . '/files'));
  $rows[] = array(t('Private file system path'), variable_get('file_private_path', ''));
  // Content
  $rows[] = array(t('Content'), format_plural(db_query('SELECT COUNT(nid) FROM {node}')->fetchField(), '1 node', '@count nodes'));
  $node_types = array();
  $result = db_query('SELECT n.type, nt.name, COUNT(n.nid) AS count FROM {node} n LEFT JOIN {node_type} nt ON n.type = nt.type GROUP BY n.type');
  foreach ($result as $record) {
    if (!$record->name) {
      $record->name = $record->type;
    }
    $node_types[$record->name] = $record;
  }
  ksort($node_types);
  foreach ($node_types as $node_type) {
    $rows[] = array(array('data' => check_plain($node_type->name), 'class' => 'text1'), format_plural($node_type->count, '1 node', '@count nodes'));
  }
  // Users
  $rows[] = array(t('Users'), format_plural(db_query('SELECT COUNT(uid) FROM {users} WHERE uid <> 0')->fetchField(), '1 account', '@count accounts'));
  $rows[] = array(array('data' => t('Active'), 'class' => 'text1'), format_plural(db_query('SELECT COUNT(uid) FROM {users} WHERE uid <> 0 AND status = 1')->fetchField(), '1 account', '@count accounts'));
  $rows[] = array(array('data' => t('Blocked'), 'class' => 'text1'), format_plural(db_query('SELECT COUNT(uid) FROM {users} WHERE uid <> 0 AND status = 0')->fetchField(), '1 account', '@count accounts'));
  // Modules
  $rows[] = array(array('data' => t('Modules'), 'class' => 'title1', 'colspan' => '2'));
  $modules = array();
  foreach (module_list() as $module) {
    $module_info = system_get_info('module', $module);
    $modules[$module_info['name']] = array(array('data' => $module_info['name'], 'class' => 'text1'), $module_info['version']);
  }
  ksort($modules);
  $rows += $modules;
  // Themes
  $rows[] = array(array('data' => t('Themes'), 'class' => 'title1', 'colspan' => '2'));
  $themes = array();
  foreach (list_themes() as $theme) {
    if ($theme->status) {
      $themes[$theme->name] = array(array('data' => $theme->info['name'], 'class' => 'text1'), $theme->info['version']);
    }
  }
  ksort($themes);
  $rows += $themes;

  $output = '<h3>' . t('Drupal') . '</h3>';
  $output .= '<p>' . t('More information about the current state of the Drupal installation can be found <a href="@drupal" title="Display current state of the Drupal installation.">here</a>.', array('@drupal' => url('admin/reports/systeminfo/drupal'))) . '</p>';
  $output .= theme('table', array('header' => array(), 'rows' => $rows, 'attributes' => array('class' => array('systeminfo', 'systeminfo_width50'))));

  return $output;
}


function systeminfo_admin_overview_php() {

  $rows = array();
  $rows[] = array(t('Version'), phpversion());
  $rows[] = array(t('Magic quotes GPC'), ini_get('magic_quotes_gpc') ? t('On') : t('Off'));
  $rows[] = array(t('Magic quotes runtime'), ini_get('magic_quotes_runtime') ? t('On') : t('Off'));
  $rows[] = array(t('Max execution time'), ini_get('max_execution_time'));
  $rows[] = array(t('Max input time'), ini_get('max_input_time'));
  $rows[] = array(t('Memory limit'), ini_get('memory_limit'));
  $rows[] = array(t('Post max size'), ini_get('post_max_size'));
  $rows[] = array(t('Register globals'), ini_get('register_globals') ? t('On') : t('Off'));
  $rows[] = array(t('Safe mode'), ini_get('safe_mode') ? t('On') : t('Off'));
  $rows[] = array(t('Session cache limiter'), ini_get('session.cache_limiter'));
  $cookie_params = session_get_cookie_params();
  $rows[] = array(t('Session cookie domain'), !empty($cookie_params['domain']) ? $cookie_params['domain'] : theme('placeholder', t('no value')));
  $rows[] = array(t('Session name'), session_name());
  $rows[] = array(t('Session save handler'), ini_get('session.save_handler'));
  $rows[] = array(t('Upload max filesize'), ini_get('upload_max_filesize'));

  $output = '<h3>' . t('PHP') . '</h3>';
  $output .= '<p>' . t('More information about the current state of PHP can be found <a href="@php" title="Display current state of PHP.">here</a>.', array('@php' => url('admin/reports/systeminfo/php'))) . '</p>';
  $output .= theme('table', array('header' => array(), 'rows' => $rows, 'attributes' => array('class' => array('systeminfo', 'systeminfo_width50'))));

  return $output;
}


function systeminfo_admin_overview_database() {

  $rows = array();



  $output = '<h3>' . t('Database system') . '</h3>';
  $output .= '<p>' . t('More information about the current state of the database system can be found <a href="@database" title="Display current state the database system.">here</a>.', array('@database' => url('admin/reports/systeminfo/database'))) . '</p>';
  $output .= theme('table', array('header' => array(), 'rows' => $rows, 'attributes' => array('class' => array('systeminfo', 'systeminfo_width50'))));

  return $output;
}


/**
 * Menu callback; configues the display.
 */
function systeminfo_admin_settings($form, &$form_state) {

  // Drupal
  $form['drupal'] = array(
    '#type' => 'fieldset',
    '#title' => 'Drupal',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['drupal']['systeminfo_drupal_modules_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Modules list'),
    '#default_value' => variable_get('systeminfo_drupal_modules_sort', 'name'),
    '#options' => array(
      'name' => t("Ascending sorted by module's name."),
      'filename' => t("Ascending sorted by module's filename."),
      'callup' => t("Ascending sorted by module's call-up."),
    ),
  );
  $form['drupal']['systeminfo_drupal_themes_sort'] = array(
    '#type' => 'radios',
    '#title' => t('Themes list'),
    '#default_value' => variable_get('systeminfo_drupal_themes_sort', 'name'),
    '#options' => array(
      'name' => t("Ascending sorted by theme's name."),
      'filename' => t("Ascending sorted by theme's filename."),
    ),
  );
  // PHP
  $form['php'] = array(
    '#type' => 'fieldset',
    '#title' => 'PHP',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['php']['systeminfo_php_phpinfo_parameter'] = array(
    '#type' => 'radios',
    '#title' => t('PHPinfo'),
    '#description' => t('Outputs a large amount of information about the current state of PHP.'),
    '#default_value' => variable_get('systeminfo_php_phpinfo_parameter', INFO_ALL),
    '#options' => array(
      INFO_GENERAL => t('The configuration line, php.ini location, build date, Web Server, System and more.'),
      INFO_CONFIGURATION => t('Current Local and Master values for PHP directives.'),
      INFO_MODULES => t('Loaded modules and their respective settings.'),
      INFO_ENVIRONMENT => t("Environment Variable information that's also available in \$_ENV."),
      INFO_VARIABLES => t('Shows all predefined variables from EGPCS (Environment, GET, POST, Cookie, Server).'),
      INFO_ALL => t('Shows all of the above.'),
    ),
  );

  // Buttons
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/reports/systeminfo'),
  );

  $form['#submit'][] = 'system_settings_form_submit';
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  return $form;
}
