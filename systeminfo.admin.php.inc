<?php

/**
 * @file
 * Admin PHP page callbacks for the systeminfo module.
 */


/**
 * Menu callback; displays PHP page.
 */
function systeminfo_admin_php() {

  if (function_exists('phpinfo')) {

    phpinfo(variable_get('systeminfo_php_phpinfo_parameter', INFO_ALL));
    exit();
  }
  else {

    return '<p>' . t('The phpinfo() function has been disabled for security reasons. To see your server\'s phpinfo() information, change your PHP settings or contact your server administrator. For more information, <a href="@phpinfo">Enabling and disabling phpinfo()</a> handbook page.', array('@phpinfo' => 'http://drupal.org/node/243993')) . '</p>';
  }
}
